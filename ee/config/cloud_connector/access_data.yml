---
# ============================
# CLOUD CONNECTOR ACCESS DATA
# ============================
#
services: # Cloud connector features (i.e. code_suggestions, duo_chat...)
  code_suggestions:
    # The name of the backend who is serving this service. The name is used as a token audience claim.
    backend: 'gitlab-ai-gateway'
    # Represents the cut-off date when the service is no longer available for free (beta)
    # If it's not set, the service is available for free (in beta) by default
    # During free access period, all unit primitives will be available for free
    cut_off_date: 2024-02-15 00:00:00 UTC
    # The group of UP that are bundled and sold together.
    # Example: code_suggestions and duo_chat are 2 UP sold together under DUO_PRO add-on.
    # Unit primitives from different services can be bundled and sold together under same add-on.
    # The same UP can be also bundled with different add-ons.
    bundled_with:
      duo_pro:
        # The smallest logical feature that a permission/access scope can govern. Currently,
        # we have duo_chat and code_suggestions as UP. In the next iteration, we could split duo_chat to smaller
        # unit_primitives. At the moment, unit_primitive name is used as a scope when ServiceToken is being issued
        unit_primitives:
          - code_suggestions
      duo_enterprise:
        unit_primitives:
          - code_suggestions
  duo_chat:
    backend: 'gitlab-ai-gateway'
    # Represents the cut-off date when the service requires addon seat purchase.
    cut_off_date: 2024-10-17 00:00:00 UTC
    bundled_with:
      duo_pro:
        unit_primitives:
          - duo_chat
          - documentation_search
      duo_enterprise:
        unit_primitives:
          - duo_chat
          - documentation_search
          - ask_build
          - ask_commit
          - ask_epic
          - ask_issue
          - ask_merge_request
  generate_description:
    backend: 'gitlab-ai-gateway'
    bundled_with:
      duo_enterprise:
        unit_primitives:
          - generate_issue_description
  anthropic_proxy:
    backend: 'gitlab-ai-gateway'
    cut_off_date: 2024-10-17 00:00:00 UTC
    bundled_with:
      duo_enterprise:
        unit_primitives:
          - categorize_duo_chat_question
          - documentation_search
          - explain_vulnerability
          - resolve_vulnerability
          - generate_issue_description
          - glab_ask_git_command
          - summarize_issue_discussions
          - generate_commit_message
          - review_merge_request
          - summarize_review
  vertex_ai_proxy:
    backend: 'gitlab-ai-gateway'
    cut_off_date: 2024-10-17 00:00:00 UTC
    bundled_with:
      duo_enterprise:
        unit_primitives:
          - documentation_search
          - duo_chat
          - explain_code
          - explain_vulnerability
          - generate_commit_message
          - generate_cube_query
          - glab_ask_git_command
          - resolve_vulnerability
          - semantic_search_issue
          - summarize_issue_discussions
          - summarize_merge_request
  explain_vulnerability:
    backend: 'gitlab-ai-gateway'
    bundled_with:
      duo_enterprise:
        unit_primitives:
          - explain_vulnerability
  glab_ask_git_command:
    backend: 'gitlab-ai-gateway'
    cut_off_date: 2024-10-17 00:00:00 UTC
    bundled_with:
      duo_enterprise:
        unit_primitives:
          - glab_ask_git_command
  resolve_vulnerability:
    backend: 'gitlab-ai-gateway'
    bundled_with:
      duo_enterprise:
        unit_primitives:
          - resolve_vulnerability
  troubleshoot_job:
    backend: 'gitlab-ai-gateway'
    cut_off_date: 2024-10-17 00:00:00 UTC
    bundled_with:
      duo_enterprise:
        unit_primitives:
          - troubleshoot_job
  self_hosted_models:
    backend: 'gitlab-ai-gateway'
    cut_off_date: 2024-10-17 00:00:00 UTC
    bundled_with:
      duo_enterprise:
        unit_primitives:
          - code_suggestions
          - duo_chat
  generate_commit_message:
    backend: 'gitlab-ai-gateway'
    cut_off_date: 2024-10-17 00:00:00 UTC
    bundled_with:
      duo_enterprise:
        unit_primitives:
          - generate_commit_message
  summarize_comments:
    backend: 'gitlab-ai-gateway'
    cut_off_date: 2024-10-17 00:00:00 UTC
    bundled_with:
      duo_enterprise:
        unit_primitives:
          - summarize_comments
  observability_all:
    backend: 'gitlab-observability-backend'
    bundled_with:
      observability:
        unit_primitives:
          - observability_all
  sast:
    backend: 'gitlab-security-gateway'
    bundled_with:
      _irrelevant_: # not checked when cut_off_date is null
        unit_primitives:
          - security_scans
  duo_workflow:
    backend: 'gitlab-duo-workflow-service'
    bundled_with:
      _irrelevant: # not checked when cut_off_date is null
        unit_primitives:
          - duo_workflow_execute_workflow
          - duo_workflow_generate_token
